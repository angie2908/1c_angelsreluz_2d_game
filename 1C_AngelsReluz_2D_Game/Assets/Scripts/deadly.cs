﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class deadly : MonoBehaviour{

     private bool dirRight = true;
     public float speed = 2.0f;
     public float limitsx;
     public float limitsxx;
 
     void Update () {
         if (dirRight)
             transform.Translate (Vector2.right * speed * Time.deltaTime);
         else
             transform.Translate (-Vector2.right * speed * Time.deltaTime);
         
         if(transform.position.x >= limitsx) {
             dirRight = false;
         }
         
         if(transform.position.x <= limitsxx) {
             dirRight = true;
         }
     } 
}